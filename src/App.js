import React, { Component } from 'react'

import GuestItem from './GuestItem';

import guests from './guests.json';
import './App.css';

const converted_guests = guests.map( user => {
  return({
    user,
    arrived: false
  });
})

console.log( guests, converted_guests );

class App extends Component {

  state = {
    guests: converted_guests,
    filtred_guests: [],
    value: ""
  }

  changeUserStatus = ( index ) => ( event ) => {
      let changedUsers = this.state.guests.map( guest => {
        if( guest.user.index === index ){
          guest.arrived = !guest.arrived;
        }
        return guest;
      });
      this.setState({ guests: changedUsers });
  }

  changeHandler = (e) => {
    const query = e.target.value.toLowerCase();
    const filtredUsers = this.state.guests.filter(( guest => guest.user.name.toLowerCase().indexOf( query ) !== -1) ,  (guest => guest.user.address.toLowerCase().indexOf( query ) !== -1))

    this.setState({
      value: query,
      filtred_guests: filtredUsers
    });
    /*if (this.filtred_guests == null){
      return
    }*/

  }


  render = () => {

    const { changeUserStatus, changeHandler } = this;
    const { guests, filtred_guests, value } = this.state;

    let data = guests;
    if( filtred_guests.length > 0 ){
      data = filtred_guests;
    }
    if(filtred_guests.length == 0 && value != null){
      console.log( 'Nema');

    }

    return(
      <div>
        <h1> App </h1>
        <input value={value} onChange={changeHandler} />
        <ul>
          {
            data.map( guest => {
              return(
                <GuestItem
                  changeStatus={changeUserStatus}
                  guest={guest}
                />
              )
            })
          }
        </ul>
      </div>
    );

  }

}

export default App;
