import React, { Component } from 'react'


class GuestItem extends Component {
    render = () => {
        const { guest, changeStatus } = this.props;

        let { arrived, user } = guest;
        return(
            <li
                className={  arrived ? 'status__arrived' : 'status_notarrived' }
            >
                <span>{user.name}</span>
                <span>{user.address}</span>
                <button onClick={ changeStatus(user.index) }>
                {
                    arrived ? 'Прибыл' : 'Отсутствует'
                }
                </button>
            </li>
        );
    }
}

export default GuestItem;
